# This is an auto generated Dockerfile for ros:indigo-desktop-full
# generated from templates/docker_images/create_ros_image.Dockerfile.em
# generated on 2017-01-27 02:33:52 +0000
FROM osrf/ros:indigo-desktop

# install ros packages
RUN apt-get update && apt-get install -y \
    ros-indigo-desktop-full=1.1.4-0* ros-indigo-moveit 
RUN apt-get install -y python-pip gfortran libatlas-base-dev python-numpy python-scipy \
    && rm -rf /var/lib/apt/lists/*

#install Lasagne and Theano
RUN pip install --upgrade https://github.com/Theano/Theano/archive/master.zip
RUN pip install --upgrade https://github.com/Lasagne/Lasagne/archive/master.zip

#Gazebo ros controll
RUN apt-get update && apt-get install -y ros-indigo-ros-control xvfb ros-indigo-robot-state-publisher

RUN mkdir -p  /usr/catkin_ws/src && cd /usr/catkin_ws/src
RUN /bin/bash -c "source /opt/ros/indigo/setup.bash"
#RUN git clone https://jakubWojciechowski@bitbucket.org/jakubWojciechowski/ur10_ddpg.git

# setup environment
EXPOSE 11345
#--------------------
# Entry point
#--------------------

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["bash"]

