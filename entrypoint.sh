#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/indigo/setup.bash"
cd "/usr/catkin_ws/src"
git clone https://bitbucket.org/jakubWojciechowski/ur10_ddpg.git
cd "/usr/catkin_ws/"
catkin_make
source "/usr/catkin_ws/devel/setup.bash"
#xvfb-run -s "-screen 0 1400x900x24" bash
roslaunch ur_gazebo ur10_joint_limited.launch gui:=false headless:=true
#roslaunch ur_gazebo ur5.launch
